package com.unus.mainhome.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {

    @RequestMapping(method = RequestMethod.GET, value = "/")
    private String helloWorld() {
        return "home";
    }

    @GetMapping("/hello")
    private String hello() {
        return "User/Hello";
    }

    @GetMapping("/login")
    private String login() {
        return "User/login";
    }
}
