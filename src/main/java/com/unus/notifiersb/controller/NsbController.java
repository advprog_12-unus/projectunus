package com.unus.notifiersb.controller;

import com.unus.account.repository.UserRepository;
import com.unus.notifiersb.core.NotificationBase;
import com.unus.notifiersb.repository.NotificationRepository;
import com.unus.notifiersb.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class NsbController {
    @Autowired
    private NotificationService notifService;

    @Autowired
    private NotificationRepository notifRepo;

    @Autowired
    private UserRepository userRepo;

    public NsbController(NotificationService notificationService) {
        this.notifService = notificationService;
    }

    // Route ke split bill
    @GetMapping("/split-bill")
    private String splitBill() {
        return "Notification/SplitBill";
    }

    // Route ke tampilan notifikasi
    @GetMapping("/notif")
    private String notif(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String uname = auth.getName();
        model.addAttribute("notifList", userRepo.findByUserName(uname).getNotification());
        return "Notification/Notification";
    }

    // Route ke form notifikasi
    @GetMapping("/notif/create-notif")
    public String createNotif(Model model) {
        model.addAttribute("newNotif", new NotificationBase());
        return "Notification/notifForm";
    }

    /** Route untuk menyimpan notifikasi yang ditambahkan dan kembali ke halaman notifikasi. */
    @PostMapping("/notif/add-notif")
    public String addNotif(@ModelAttribute("notificat") NotificationBase notifica) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String uname = auth.getName();
        notifica.setUser(userRepo.findByUserName(uname));
        notifService.register(notifica);
        return "redirect:/notif";
    }

    // Route untuk membuang notifikasi yang dipilih
    @GetMapping("/notif/delete/{id}")
    public String deleteNotif(@PathVariable Long id) {
        notifService.deleteNotif(id);
        return "redirect:/notif";
    }

}
