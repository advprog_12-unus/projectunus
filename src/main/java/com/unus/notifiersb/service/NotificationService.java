package com.unus.notifiersb.service;

import com.unus.notifiersb.core.NotificationBase;

import java.util.List;

public interface NotificationService {
    public void deleteNotif(Long id); //delete

    public NotificationBase register(NotificationBase notif); //create

    public List<NotificationBase> findAllNotif();
}
