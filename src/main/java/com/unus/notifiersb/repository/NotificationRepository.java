package com.unus.notifiersb.repository;

import com.unus.notifiersb.core.NotificationBase;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationRepository extends JpaRepository<NotificationBase, Long> {
}
