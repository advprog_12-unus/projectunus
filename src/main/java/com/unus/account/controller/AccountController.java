package com.unus.account.controller;

import com.unus.account.model.UserDtoBase;
import com.unus.account.service.UserServiceImpl;
import com.unus.account.service.error.UsernameAlreadyExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class AccountController {
    
    @Autowired
    private UserServiceImpl userService;

    /**
     * Return registration form template with a User Data Transfer Object.
     * @param request http request
     * @param model model to return
     * @return a template
     */
    @GetMapping("/register")
    public String registrationForm(final HttpServletRequest request, final Model model) {
        final UserDtoBase userDto = new UserDtoBase();
        model.addAttribute("user", userDto);
        model.addAttribute("message", "");
        return "User/register";
    }

    /**
     * Post method to register user.
     * @param userDto User Data Transfer Object for creating User
     * @param request http request
     * @param error errors
     * @return a model and view object
     */
    @PostMapping("/register")
    public ModelAndView registerUser(@ModelAttribute("user") @Valid final UserDtoBase userDto, 
                                    final HttpServletRequest request, 
                                    final Errors error) {
        try {
            userService.registerUser(userDto);
        } catch (UsernameAlreadyExistException uaeEx) {
            ModelAndView mav = new ModelAndView("User/register");
            mav.addObject("user", userDto);
            mav.addObject("message", "An account for that username already exists.");
            return mav;
        }
        return new ModelAndView("User/successRegister", "user", userDto);
    }
}
