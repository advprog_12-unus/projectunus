package com.unus.dompet.repository;

import com.unus.dompet.core.DompetState;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StateRepository extends JpaRepository<DompetState, Long> {
}
