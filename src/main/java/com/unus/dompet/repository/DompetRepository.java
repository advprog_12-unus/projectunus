package com.unus.dompet.repository;

import com.unus.dompet.core.Dompet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DompetRepository extends JpaRepository<Dompet, Long> {
}
