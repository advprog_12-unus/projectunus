package com.unus.dompet.core;

public class DompetLocked extends DompetState {
    DompetLocked(Dompet dompet) {
        super(dompet);
    }

    @Override
    public void changeState() {
        dompet.state = new DompetUnlocked(dompet);
    }

    @Override
    public String changeName(String name) {
        return "Invalid Command";
    }

    @Override
    public String changeDescription(String description) {
        return "Invalid Command";
    }
}