package com.unus.dompet.core;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "state")
public abstract class DompetState {

    @Id
    @Column(name = "id")
    private Long id;

    @OneToOne
    @MapsId
    Dompet dompet;
    
    DompetState(Dompet dompet) {
        this.dompet = dompet;
    }

    public abstract void changeState();

    public abstract String changeName(String name);

    public abstract String changeDescription(String description);
}