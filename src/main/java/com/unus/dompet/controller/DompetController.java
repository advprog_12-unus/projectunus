package com.unus.dompet.controller;

import com.unus.account.repository.UserRepository;
import com.unus.dompet.core.Dompet;
import com.unus.dompet.service.DompetService;
import com.unus.notifiersb.core.NotificationBase;
import com.unus.notifiersb.repository.NotificationRepository;
import com.unus.notifiersb.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class DompetController {

    @Autowired
    private DompetService dompetService;

    @Autowired
    private UserRepository userRepo;

    public DompetController(DompetService dompetService) {
        this.dompetService = dompetService;
    }

    @GetMapping("/dompet")
    private String dompet(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String uname = auth.getName();
        model.addAttribute("dompetList", userRepo.findByUserName(uname).getDompet());
        return "Dompet/dompet";
    }

    @GetMapping("/dompet/createdompet")
    public String createDompet(Model model) {
        model.addAttribute("newDompet", new Dompet("",""));
        return "Dompet/adddompet";
    }    

    /**
    * Make mapping for add dompet.
    **/
    @PostMapping("/dompet/add-dompet")
    public String addDompet(@ModelAttribute("Dompet") Dompet dompet) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String uname = auth.getName();
        dompet.setUser(userRepo.findByUserName(uname));
        dompetService.register(dompet);
        return "redirect:/dompet";
    }

    @GetMapping("dompet/delete/{id}")
    public String deleteDompet(@PathVariable Long id) {
        dompetService.deleteDompet(id);
        return "redirect:/dompet";
    }
}
