package com.unus.dompet.service;

import com.unus.dompet.core.Dompet;
import com.unus.dompet.repository.DompetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DompetServiceImpl implements DompetService {
    @Autowired
    private DompetRepository dompetRepo;

    public DompetServiceImpl(DompetRepository dompetRepo) {
        this.dompetRepo = dompetRepo;
    }

    @Override
    public Dompet register(Dompet dompet) {
        return dompetRepo.save(dompet);
    }

    @Override
    public void deleteDompet(Long id) {
        dompetRepo.deleteById(id);
    }

    public List<Dompet> findAlldompet() {
        return dompetRepo.findAll();
    }
}
