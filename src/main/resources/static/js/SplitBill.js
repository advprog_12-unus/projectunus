$(document).ready(() =>{
    var jmlItem;
    var listPersentase = new Array();
    var listNamaBarang = new Array();
    var listNamaOrang = new Array();

    $("#btn-harga-asli").hide();
    $("#container-diskon").hide();
    $("#container-hasil").hide();

    $("#btn-jml-item").on("click",
        function(){
            jmlItem = $("#input-jml-item").val();
            if(jmlItem > 0 && jmlItem <= 100){
                let appendHTML = "";
                for(i=0; i<jmlItem; i++){
                    appendHTML += `
                <br>
                <p id="namBar${i}">Masukkan Nama Barang ke-${i+1} :</p>
                <input type="text" class="form-control nama-barang" placeholder="Masukkan teks">
                <p>Masukkan Harga Asli ke-${i+1} :</p>
                <input type="number" class="form-control input-harga-asli" placeholder="Masukkan angka">
                <p>Masukkan Pemilik Barang ke-${i+1} :</p>
                <input type="text" class="form-control nama-orang" placeholder="Masukkan teks">
                <br>
                `;
                }
                $("#container-harga-asli").html(appendHTML);
                $("#btn-harga-asli").show();
                let element = document.getElementById('namBar0');
                element.scrollIntoView();
            }else{
                $("#btn-harga-asli").hide();
                let appendHTML = "";
                appendHTML += `
                <p>Harap hanya masukkan angka 1 sampai dengan 100</p>
                `;
                $("#container-harga-asli").html(appendHTML);
            }
        }
    );

    $("#btn-harga-asli").on("click",
        function(){
            var hargaAsli = 0;
            hargaAsli.toPrecision(21);

            var listHarga = new Array();
            $('.input-harga-asli').each(function(){
                let hargaIni = parseFloat(parseFloat($(this).val()).toPrecision(21));
                listHarga.push(hargaIni);
                hargaAsli += hargaIni;
            })

            listNamaBarang = new Array();
            $('.nama-barang').each(function(){
                let barangIni = $(this).val();
                listNamaBarang.push(barangIni);
            })

            listNamaOrang = new Array();
            $('.nama-orang').each(function(){
                let orangIni = $(this).val();
                listNamaOrang.push(orangIni);
            })

            listPersentase = new Array();
            for(i=0; i<jmlItem; i++){
                listPersentase.push(parseFloat(listHarga[i] / hargaAsli).toPrecision(21));
            }
            $("#container-diskon").show();
            let element = document.getElementById('teks-harga-total');
            element.scrollIntoView();
        }
    );

    $("#btn-harga-diskon").on("click",
        function(){
            hargaTotalDiskon = parseFloat($("#input-harga-diskon").val()).toPrecision(21);
            let appendHTML = "";
            for(i=0; i<jmlItem; i++){
                let hargaDiskon = Math.ceil((listPersentase[i]*hargaTotalDiskon).toPrecision(21));
                appendHTML += `
            <li class="list-group-item">
              <p>Nama Barang: ${listNamaBarang[i]}</p>
              <p>Harga barang ke-${i+1} Setelah Diskon : ${hargaDiskon}</p>
              <p>Nama Orang: ${listNamaOrang[i]}</p>
            </li>
            `;
            }
            $("#container-harga-diskon").html(appendHTML);
            $("#container-hasil").show();
            let element = document.getElementById('card-hasil');
            element.scrollIntoView();
        }
    );

    $("#btn-reset").on("click",
        function(){
            window.location.reload();
        }
    );
})