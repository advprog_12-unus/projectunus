package com.unus.dompettest.core;

import com.unus.account.model.BaseUser;
import com.unus.dompet.core.Dompet;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class DompetTest {
    @Test
    void testobject1() {
        Dompet dompet = new Dompet("At","Uang kas ate");

        assertEquals("At", dompet.getName());
        assertEquals("Uang kas ate", dompet.getDescription());
    }

    @Test
    void testobject2() {
        Dompet dompet = new Dompet("Ate","Uang kas atee");
        dompet.setName("test");
        assertEquals("test", dompet.getName());
    }

    @Test
    void testobject3() {
        Dompet dompet = new Dompet("Ate","Halo");
        dompet.setDescription("Uang kas Ate");
        assertEquals("Uang kas Ate", dompet.getDescription());
    }

    @Test
    void teststatepattern1() {
        Dompet dompet = new Dompet("At","Uang kas ate");
        dompet.state.changeState();
        
        assertEquals(dompet.state.changeName("Hello"), dompet.getName());
    }
    
    @Test
    void teststatepattern2() {
        Dompet dompet = new Dompet("At","Uang kas ate");
        assertEquals("Invalid Command",dompet.state.changeName("Evando"));
    }

    @Test
    void teststatepattern3() {
        Dompet dompet = new Dompet("At","Uang kas ate");
        dompet.state.changeState();
        
        assertEquals(dompet.state.changeDescription("Uang Kas Evando"), dompet.getDescription());
    }

    @Test
    void teststatepattern4() {
        Dompet dompet = new Dompet("At","Uang kas ate");
        assertEquals("Invalid Command",dompet.state.changeDescription("Uang kas Evando"));
    }

    @Test
    void testtotal() {
        Dompet dompet = new Dompet("At", "Uang kas Ate");
        assertEquals(dompet.getTotal(), 0);
    }

    @Test
    void teststatepattern5() {
        Dompet dompet = new Dompet("At","Uang kas ate");
        dompet.state.changeState();
        dompet.state.changeState();
        assertEquals("Invalid Command",dompet.state.changeDescription("Uang kas Evando"));
    }

    @Test
    void getIdTest() {
        Dompet dompet = new Dompet("At","Uang kas ate");
        assertEquals(0, dompet.getId());
    }

    @Test
    void getUserTest() {
        Dompet dompet = new Dompet("At","Uang kas ate");
        BaseUser userTest = new BaseUser("testUser", "testt", "User");
        dompet.setUser(userTest);
        assertEquals(userTest, dompet.getUser());
    }
}