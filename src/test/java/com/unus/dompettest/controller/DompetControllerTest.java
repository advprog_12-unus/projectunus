package com.unus.dompettest.controller;

import com.unus.account.model.BaseUser;
import com.unus.account.repository.UserRepository;
import com.unus.account.security.UserDetailsImpl;
import com.unus.account.security.UserDetailsServiceImpl;
import com.unus.dompet.controller.DompetController;
import com.unus.dompet.repository.DompetRepository;
import com.unus.dompet.service.DompetServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(controllers = DompetController.class)
public class DompetControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserDetailsImpl userDetails;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private DompetServiceImpl notificationService;

    @MockBean
    private DompetRepository notificationRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private BaseUser user;
    
    @BeforeEach
    public void setUp() throws Exception {
        user = new BaseUser("admin", "admin123", "USER");
        userDetails = new UserDetailsImpl(user);
    }

    @Test
    public void addDompetUrlTest() throws Exception {
        mockMvc.perform(post("/dompet/add-dompet"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void deleteDompetUrlTest() throws Exception {
        mockMvc.perform(get("/dompet/delete"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser
    public void createNotifUrlTest() throws Exception {
        mockMvc.perform(get("/dompet/createdompet"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("newDompet"))
                .andExpect(view().name("Dompet/adddompet"));
    }

    @Test
    @WithMockUser(username = "admin", password = "admin123", roles = "USER")
    public void notifUrlWithAuthTest() throws Exception {
        BaseUser mockUser = Mockito.mock(BaseUser.class);
        when(userRepository.findByUserName("admin")).thenReturn(mockUser);

        mockMvc.perform(get("/dompet"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("dompetList"))
                .andExpect(view().name("Dompet/dompet"));
    }
    
}