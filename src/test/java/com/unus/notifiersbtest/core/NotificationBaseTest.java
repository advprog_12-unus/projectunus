package com.unus.notifiersbtest.core;

import com.unus.account.model.BaseUser;
import com.unus.notifiersb.core.NotificationBase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class NotificationBaseTest {
    private NotificationBase notifTest;
    private BaseUser userTest;

    @BeforeEach
    void setUp() {
        userTest = new BaseUser("testUser", "testt", "User");
        notifTest = new NotificationBase();
        notifTest.setTitle("TitleTest");
        notifTest.setDesc("DescTest");
    }

    @Test
    void setTitleTest() {
        notifTest.setTitle("Title2");
        assertEquals("Title2", notifTest.getTitle());
        notifTest.setTitle("TitleTest");
    }

    @Test
    void setDescTest() {
        notifTest.setDesc("Desc2");
        assertEquals("Desc2", notifTest.getDesc());
        notifTest.setDesc("DescTest");
    }

    @Test
    void setUserTest() {
        notifTest.setUser(userTest);
        assertEquals(userTest, notifTest.getUser());
        BaseUser userTest2 = new BaseUser("testUser2", "testt2", "User2");
        notifTest.setUser(userTest2);
        assertEquals(userTest2, notifTest.getUser());
    }

    @Test
    void getIdTest() {
        assertEquals(0, notifTest.getId());
    }

    @Test
    void getTitleTest() {
        assertEquals("TitleTest", notifTest.getTitle());
    }

    @Test
    void getDescTest() {
        assertEquals("DescTest", notifTest.getDesc());
    }

    @Test
    void getUserTest() {
        notifTest.setUser(userTest);
        assertEquals(userTest, notifTest.getUser());
    }
}