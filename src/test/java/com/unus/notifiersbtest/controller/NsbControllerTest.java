package com.unus.notifiersbtest.controller;

import com.unus.account.model.BaseUser;
import com.unus.account.repository.UserRepository;
import com.unus.account.security.UserDetailsImpl;
import com.unus.account.security.UserDetailsServiceImpl;
import com.unus.notifiersb.controller.NsbController;
import com.unus.notifiersb.repository.NotificationRepository;
import com.unus.notifiersb.service.NotificationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(controllers = NsbController.class)
public class NsbControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserDetailsImpl userDetails;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private NotificationServiceImpl notificationService;

    @MockBean
    private NotificationRepository notificationRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private BaseUser user;

    @BeforeEach
    public void setUp() throws Exception {
        user = new BaseUser("admin", "admin123", "USER");
        userDetails = new UserDetailsImpl(user);
    }

    @Test
    public void splitBillUrlTest() throws Exception {
        mockMvc.perform(get("/split-bill"))
                .andExpect(status().isOk())
                .andExpect(view().name("Notification/SplitBill"));
    }

    @Test
    public void notifUrlWithoutAuthTest() throws Exception {
        mockMvc.perform(get("/notif"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser(username = "admin", password = "admin123", roles = "USER")
    public void notifUrlWithAuthTest() throws Exception {
        BaseUser mockUser = Mockito.mock(BaseUser.class);
        when(userRepository.findByUserName("admin")).thenReturn(mockUser);

        mockMvc.perform(get("/notif"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("notifList"))
                .andExpect(view().name("Notification/Notification"));
    }

    @Test
    public void createNotifUrlWithoutAuthTest() throws Exception {
        mockMvc.perform(get("/notif/create-notif"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    public void createNotifUrlWithAuthTest() throws Exception {
        mockMvc.perform(get("/notif/create-notif"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("newNotif"))
                .andExpect(view().name("Notification/notifForm"));
    }

    @Test
    public void addNotifUrlWithoutAuthTest() throws Exception {
        mockMvc.perform(post("/notif/add-notif"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void deleteNotifUrlTest() throws Exception {
        mockMvc.perform(delete("/notif/delete/1"))
                .andExpect(status().is4xxClientError());
    }

}
