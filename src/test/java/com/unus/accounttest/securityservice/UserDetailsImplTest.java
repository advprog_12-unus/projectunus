package com.unus.accounttest.securityservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.unus.account.model.BaseUser;
import com.unus.account.model.User;
import com.unus.account.security.UserDetailsImpl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UserDetailsImplTest {
    private UserDetailsImpl userDetails;

    @BeforeEach
    public void setUp() throws Exception {
        User user = new BaseUser("admin", "admin123", "USER");
        userDetails = new UserDetailsImpl(user);
    }

    @Test
    public void getPasswordShouldReturnAString() {
        assertNotNull(userDetails.getPassword());
    }

    @Test
    public void getPasswordShouldReturnUserDetailsPassword(){
        assertEquals("admin123", userDetails.getPassword());
    }

    @Test
    public void getUsernameShouldNotReturnNull() {
        assertNotNull(userDetails.getUsername());
    }

    @Test
    public void getUsernameShouldReturnUserDetailsUsername(){
        assertEquals("admin", userDetails.getUsername());
    }

    @Test
    public void isAccountNotExpiredSHouldReturnTrue(){
        assertTrue(userDetails.isAccountNonExpired());
    }

    @Test
    public void isAccountNotLockedShouldReturnTrue() {
        assertTrue(userDetails.isAccountNonLocked());
    }

    @Test
    public void isCredentialsNotExpiredShouldReturnTrue() {
        assertTrue(userDetails.isCredentialsNonExpired());
    }

    @Test
    public void isEnableShouldReturnTrue() {
        assertTrue(userDetails.isEnabled());
    }

    @Test
    public void getAuthoritiesShouldNotReturnNull(){
        assertNotNull(userDetails.getAuthorities());
    }
}
