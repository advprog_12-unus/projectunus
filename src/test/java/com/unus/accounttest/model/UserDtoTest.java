package com.unus.accounttest.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.unus.account.model.UserDtoBase;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UserDtoTest {

    private UserDtoBase userDto;

    @BeforeEach
    public void setup() throws Exception {
        userDto = new UserDtoBase();
    }

    @Test
    public void GetUsernameShouldReturnNull(){
        assertNull(userDto.getUsername());        
    }

    @Test
    public void setUsernameShouldChangeUserDtoUsername(){
        userDto.setUsername("test");
        assertEquals("test", userDto.getUsername());
    }

    @Test
    public void GetPasswordShouldReturnNull(){
        assertNull(userDto.getPassword());
    }

    @Test
    public void SetPasswordShouldChangeUserDtoPassword(){
        userDto.setPassword("password");
        assertEquals("password", userDto.getPassword());
    }

    @Test
    public void GetMatchingPasswordShouldReturnNull(){
        assertNull(userDto.getMatchingPassword());
    }

    @Test
    public void SetMatchingPasswordshouldChangeUserDtoMatchingPassword(){
        userDto.setMatchingPassword("matchingPassword");
        assertEquals("matchingPassword", userDto.getMatchingPassword());
    }
}