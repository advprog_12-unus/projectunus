package com.unus.accounttest.model;

import com.unus.account.model.BaseUser;
import com.unus.account.model.User;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class BaseUserTest {
    private BaseUser user;

    @BeforeEach
    public void setUp() throws Exception {
        user = new BaseUser("admin", "admin123", "USER");
    }

    @Test
    public void getPasswordShouldReturnAString() {
        assertNotNull(user.getPassword());
    }

    @Test
    public void getPasswordShouldReturnUserDetailsPassword(){
        assertEquals("admin123", user.getPassword());
    }

    @Test
    public void getUsernameShouldNotReturnNull() {
        assertNotNull(user.getUserName());
    }

    @Test
    public void getUsernameShouldReturnUserDetailsUsername(){
        assertEquals("admin", user.getUserName());
    }

    @Test
    public void getRolesShouldNotReturnNull() { assertNotNull(user.getRoles());}

    @Test
    public void getRolesShouldReturnListOfRoles() { assertEquals("USER", user.getRoles());}

    @Test
    public void getRolesListShouldNotReturnNull() { assertNotNull(user.getRolesList());}

    @Test
    public void getRolesListShouldReturnListOfRoles() {
        List<String> expected = new ArrayList<String>();
        expected.add("USER");
        assertEquals(expected, user.getRolesList());
    }

    @Test
    public void getRolesListShouldReturnANewListOfRolesIfRolesLessThanOne(){
        User user1 = new BaseUser("saya", "ganteng","");
        assertEquals(new ArrayList<>(), user1.getRolesList());
    }

    @Test
    public void getNotificationShouldReturnAListOfNotifications(){
        assertNull(user.getNotification());
    }
}
