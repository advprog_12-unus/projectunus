package com.unus.accounttest.controller;

import com.unus.account.controller.AccountController;
import com.unus.account.model.BaseUser;
import com.unus.account.model.UserDtoBase;
import com.unus.account.repository.UserRepository;
import com.unus.account.security.UserDetailsImpl;
import com.unus.account.security.UserDetailsServiceImpl;
import com.unus.account.service.UserServiceImpl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import javax.servlet.http.HttpServletRequest;

@WebMvcTest(controllers = AccountController.class)
public class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HttpServletRequest request;

    @MockBean
    private UserServiceImpl userService;

    @MockBean
    private UserDetailsImpl userDetails;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private BaseUser user;

    @MockBean
    private UserDtoBase userDto;

    @Test
    public void GetRegisterTest() throws Exception {
        mockMvc
            .perform(get("/register"))
            .andExpect(status().isOk())
            .andExpect(model().attributeExists("user"))
            .andExpect(model().attribute("message", equalTo("")))
            .andExpect(view().name("User/register"));
    }

    @Test
    public void PostRegisterTest() throws Exception {
        UserDtoBase user = new UserDtoBase();
        user.setUsername("test");
        user.setPassword("password");
        user.setMatchingPassword("password");
        ResultActions resultActions = mockMvc
                .perform(post("/register")
                        .param("username", user.getUsername())
                        .param("password", user.getPassword())
                        .param("matchingPassword", user.getMatchingPassword())
                );
        resultActions.andExpect(status().is4xxClientError());
    }
}