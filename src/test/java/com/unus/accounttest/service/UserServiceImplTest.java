package com.unus.accounttest.service;

import static org.assertj.core.api.Assertions.catchThrowableOfType;

import com.unus.account.model.UserDtoBase;
import com.unus.account.repository.UserRepository;
import com.unus.account.service.UserServiceImpl;
import com.unus.account.service.error.UsernameAlreadyExistException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @Mock
    private UserRepository userRespository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserServiceImpl userService;

    private UserDtoBase userDto;

    @BeforeEach
    public void setUp() throws Exception {
        userDto = new UserDtoBase();
        userDto.setUsername("user1");
        userDto.setPassword("password");
        userDto.setMatchingPassword("password");
    }

    @Test
    public void registerUserShouldReturnExceptionIfUsernameExists() throws Exception {
        catchThrowableOfType(
                () -> {userService.registerUser(userDto);}, UsernameAlreadyExistException.class
        );
    }
}